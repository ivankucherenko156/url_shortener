# URL Shortener

## Installation

### System requirements

    sudo apt-get i nstall python3.9 python-dev python-virtualenv python-pip

### Project
    git clone https://bitbucket.org/ivankucherenko156/url_shortener.git
    cd url_shortener && make

### Run project
    make run

### Code formatter
Black https://github.com/psf/black

    make format dir={`Full path to source file`}

