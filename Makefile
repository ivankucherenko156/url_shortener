SHELL := /bin/bash
SYSTEM_PYTHON=python3.9
VENV_DIR=venv
PIP=$(VENV_DIR)/bin/pip3
ACTIVATE=$(VENV_DIR)/bin/activate

.PHONY: all clean test run install

all: clean install

install:
	$(SYSTEM_PYTHON) -m venv $(VENV_DIR)
	$(PIP) install -r requirements.txt
	source $(ACTIVATE)

run:
	$(SYSTEM_PYTHON) manage.py runserver

test:
	$(SYSTEM_PYTHON) manage.py test

migrate:
	$(SYSTEM_PYTHON) manage.py makemigrations shortener && $(SYSTEM_PYTHON) manage.py migrate

format:
	$(VENV_DIR)/bin/black $(dir)

clean:
	rm -rf venv
	find . -type f -name '*.pyc' -delete
