from django.urls import path
from django.contrib import admin

from url_shortener.shortener.views import MainView, RedirectToView, ShortenView

app_name = "shortener"
urlpatterns = [
    path("admin/", admin.site.urls),
    path("shorten", ShortenView.as_view(), name="shorten"),
    path("<int:url_id>", RedirectToView.as_view(), name="redirect_to"),
    path("", MainView.as_view(), name="main"),
]
