from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.views.generic import TemplateView, RedirectView
from django.shortcuts import render, redirect, get_object_or_404


from .forms import URLForm
from .models import StoredURL


class MainView(TemplateView):
    form_class = URLForm
    http_method_names = ("get",)
    template_name = "main.html"

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {"form": form})


class ShortenView(TemplateView):
    form_class = URLForm
    http_method_names = ("post",)
    template_name = "result.html"

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if form.is_valid():
            input_url = request.POST.get("url")
            stored_url, _ = StoredURL.objects.get_or_create(original_url=input_url)

            return render(
                request,
                self.template_name,
                {
                    "domain": get_current_site(request),
                    "url_id": stored_url.id,
                    "transition_count": stored_url.transitions_counter,
                },
            )
        else:
            messages.error(request, form.errors)
            return redirect("main")


class RedirectToView(RedirectView):
    http_method_names = ("get",)

    def get_redirect_url(self, *args, **kwargs):
        stored_url = get_object_or_404(StoredURL, pk=kwargs.get("url_id"))
        stored_url.increase_transitions_counter()
        self.url = self._normalize_url(stored_url.original_url)
        return super().get_redirect_url(*args, **kwargs)

    @staticmethod
    def _normalize_url(url):
        return url if url.startswith("http://") or url.startswith("https://") else f"https://{url}"
