from django.db import models
from django.utils.timezone import now


class StoredURL(models.Model):
    original_url = models.CharField(max_length=400, unique=True)
    transitions_counter = models.IntegerField(default=0, blank=True)
    pub_date = models.DateTimeField("date published", default=now)

    def increase_transitions_counter(self):
        self.transitions_counter += 1
        self.save(update_fields=["transitions_counter"])
