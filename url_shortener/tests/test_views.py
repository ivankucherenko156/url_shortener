from unittest.mock import patch, MagicMock
from django.test import RequestFactory, SimpleTestCase, Client
from url_shortener.shortener.views import MainView, RedirectToView, ShortenView


class MainViewTest(SimpleTestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    def test_should_return_200_for_correct_get(self):
        request = self.factory.get('/')
        response = MainView.as_view()(request)
        self.assertEqual(response.status_code, 200)

    def test_should_return_405_for_wrong_http_method(self):
        request = self.factory.post('/')
        response = MainView.as_view()(request)
        self.assertEqual(response.status_code, 405)

    def test_should_render_main_page(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'main.html')


class RedirectToViewTest(SimpleTestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    @patch('url_shortener.shortener.views.get_object_or_404')
    def test_should_redirect_when_get(self, mocked_get_object_or_404):
        mocked_stored_url = MagicMock()
        mocked_stored_url.original_url = "https://google.com"
        mocked_get_object_or_404.return_value = mocked_stored_url

        response = self.client.get("/7")
        self.assertEqual(response.status_code, 302)

    @patch('url_shortener.shortener.views.get_object_or_404')
    def test_should_redirect_to_original_url(self, mocked_get_object_or_404):
        mocked_stored_url = MagicMock()
        mocked_stored_url.original_url = "https://google.com"
        mocked_get_object_or_404.return_value = mocked_stored_url

        view = RedirectToView()
        request = self.factory.get('/7')
        view.setup(request)
        view.get_redirect_url()
        self.assertEqual(view.url, mocked_stored_url.original_url)

    @patch('url_shortener.shortener.views.get_object_or_404')
    def test_should_return_405_for_wrong_http_method(self, mocked_get_object_or_404):
        mocked_stored_url = MagicMock()
        mocked_stored_url.original_url = "https://google.com"
        mocked_get_object_or_404.return_value = mocked_stored_url

        request = self.factory.post('/7')
        response = RedirectToView.as_view()(request)
        self.assertEqual(response.status_code, 405)

    @patch('url_shortener.shortener.views.get_object_or_404')
    def test_should_increase_transition_counter_when_redirect(self, mocked_get_object_or_404):
        mocked_stored_url = MagicMock()
        mocked_stored_url.original_url = "https://google.com"
        mocked_stored_url.increase_transitions_counter = MagicMock()
        mocked_get_object_or_404.return_value = mocked_stored_url

        self.client.get("/7")
        self.assertEqual(mocked_stored_url.increase_transitions_counter.called, True)

    def test_normalize_url_function_should_add_protocol_if_absent(self):
        view = RedirectToView()
        normalized_url = view._normalize_url("google.com")
        self.assertEqual(normalized_url, "https://google.com")

    def test_normalize_url_function_should_leave_unchanged_if_protocol_present(self):
        view = RedirectToView()
        base_url = "https://google.com"
        normalized_url = view._normalize_url(base_url)
        self.assertEqual(normalized_url, base_url)

    def test_normalize_url_function_should_check_if_the_protocol_is_correct(self):
        view = RedirectToView()
        base_url = "http.google.com"
        normalized_url = view._normalize_url(base_url)
        self.assertEqual(normalized_url, "https://http.google.com")


class ShortenViewTest(SimpleTestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    def test_should_store_new_url_to_db(self):
        fake_new_stored_url = MagicMock()
        fake_new_stored_url.id = 7
        mock_get_or_create = MagicMock()
        mock_get_or_create.return_value = (fake_new_stored_url, False)

        with patch('url_shortener.shortener.models.StoredURL.objects.get_or_create', mock_get_or_create) as CreateMock:
            with patch('url_shortener.shortener.forms.URLForm.is_valid') as FormIsValidMock:
                FormIsValidMock.return_value = True
                request = self.factory.post('/shorten')
                ShortenView.as_view()(request)
                self.assertEqual(CreateMock.called, True)

    def test_should_not_store_new_url_to_db_if_form_is_not_valid(self):
        fake_new_stored_url = MagicMock()
        fake_new_stored_url.id = 7
        mock_get_or_create = MagicMock()
        mock_get_or_create.return_value = (fake_new_stored_url, False)

        with patch('url_shortener.shortener.models.StoredURL.objects.get_or_create', mock_get_or_create) as CreateMock:
            with patch('url_shortener.shortener.forms.URLForm.is_valid') as FormIsValidMock:
                with patch('django.contrib.messages.error', MagicMock()):
                    FormIsValidMock.return_value = False
                    request = self.factory.post('/shorten')
                    ShortenView.as_view()(request)
                    self.assertEqual(CreateMock.called, False)

    def test_should_set_up_error_if_form_is_not_valid(self):
        fake_new_stored_url = MagicMock()
        fake_new_stored_url.id = 7
        mock_get_or_create = MagicMock()
        mock_get_or_create.return_value = (fake_new_stored_url, False)

        with patch('url_shortener.shortener.models.StoredURL.objects.get_or_create', mock_get_or_create) as CreateMock:
            with patch('url_shortener.shortener.forms.URLForm.is_valid') as FormIsValidMock:
                with patch('django.contrib.messages.error') as MessagesErrorMock:
                    FormIsValidMock.return_value = False
                    request = self.factory.post('/shorten')
                    ShortenView.as_view()(request)
                    self.assertEqual(MessagesErrorMock.called, True)
