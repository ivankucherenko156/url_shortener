from django.test import TestCase

from url_shortener.shortener.forms import URLForm


class URLFormTests(TestCase):

    def test_url_field_is_required(self):
        form = URLForm(data={"url": ""})
        self.assertEqual(
            form.errors["url"], ["This field is required."]
        )

    def test_ulr_field_is_valid(self):
        form = URLForm(data={"url": "fffff"})
        self.assertEqual(
            form.errors["url"], ["Enter a valid URL."]
        )
