import datetime

from dateutil import tz
from django.test import TestCase
from freezegun import freeze_time

from url_shortener.shortener.models import StoredURL


class StoredURLTestCase(TestCase):
    def setUp(self):
        StoredURL.objects.create(original_url="test.com")

    def test_default_transitional_counts_should_be_eq_zero(self):
        test = StoredURL.objects.get(original_url="test.com")
        self.assertEqual(test.transitions_counter, 0)

    @freeze_time("1955-11-12 05:22:30")
    def test_pub_date_should_be_as_now(self):
        StoredURL.objects.create(original_url="test2.com")
        test2 = StoredURL.objects.get(original_url="test2.com")
        utc = tz.gettz('UTC')
        self.assertEqual(test2.pub_date, datetime.datetime(1955, 11, 12, 5, 22, 30, tzinfo=utc))

    def test_increase_transitions_counter(self):
        test = StoredURL.objects.get(original_url="test.com")
        test.transitions_counter = 4
        test.increase_transitions_counter()
        self.assertEqual(test.transitions_counter, 5)
